﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;

namespace CloneFromGit
{
    class Git
    {
        public static void CloneBrunchFromGit(string recipeName, string branchName)
        {
            var sourceUrl = @"https://github.com/aleksandra-semykina/NewRepository";
            var localRepo = @"C:\work\NewRepository";

            var repo = new Repository(localRepo);

            if (repo.Branches.Any(branch => branch.FriendlyName == branchName))
            {
                var targetDir = @"E:\Recipes\" +recipeName + "_"+ branchName + @"\";
                if (!Directory.Exists(targetDir))
                {
                    
                    Tag tag = repo.Tags["v1.0.0"];
                    Commit taggedCommit = repo.Commits.FirstOrDefault();
                    foreach (Commit commit in repo.Commits)
                    {
                        if (commit.Id == tag.Target.Id)
                            taggedCommit = commit;
                    }
                    
                    repo.CreateBranch("newBranch", taggedCommit);
                    Branch localBranch = repo.Branches["newBranch"];
                    Remote remote = repo.Network.Remotes["origin"];

                    repo.Branches.Update(localBranch,
                        b => b.Remote = remote.Name,
                        b => b.UpstreamBranch = localBranch.CanonicalName
                        );

                    Repository.Clone(sourceUrl, targetDir, new CloneOptions { BranchName = "newBranch" });

                    
                    Console.WriteLine("repository successfully cloned");
                }
                else
                {
                    Console.WriteLine("repository has already been cloned");
                }
            }
            else
            {
                Console.WriteLine("Branch wasn't found");
            }
        }
    }
}